Role Name
=========

This role has been developed, and is intended, for Debian GNU/Linux systems.

Its purpose is very simple -- set the desired hostname of the system and update
its domain name in `/etc/hosts` for `127.0.1.1`.


Role Variables
--------------

* `hostname` -- the desired hostname
* `new_domain` -- the desired domain.
* `old_domain` -- the domain that is being replaced with `new_domain`

Example Playbook
----------------

In your `requirements.yml`:
```yaml
- src: git+https://gitlab.com/prezu-debian/hostname-debian
  version: main
  name: hostname-debian
```

And then remember to install it:
```bash
$ ansible-galaxy install -r requirements.yml
```

And finally in your playbook:
```yaml
  - hosts: servers
    roles:
      - role: hostname-debian
        vars:
          hostname: webserver1
          new_domain: example.com
          old_domain: local
```

License
-------

GPLv3
